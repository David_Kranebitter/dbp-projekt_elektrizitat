package Frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import Models.Country;

public class ShowData extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private ArrayList<Country> list;
	private Object[][] dataList;

	/**
	 * Create the frame.
	 */
	public ShowData(ArrayList<Country> list) {
		setTitle("Data");
		this.list = list;

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 650, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 632, 253);
		contentPane.add(panel);
		panel.setLayout(null);
		
		dataList = new Object[60][3];
		for (int i = 0; i < list.size(); i++) {
			dataList[i][0] = list.get(i).getDate();
			dataList[i][1] = list.get(i).getCountryName();
			dataList[i][2] = list.get(i).getValue();
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 608, 227);
		panel.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setModel(new DefaultTableModel(dataList, new String[] { "TIME", "GEO", "VALUE" }));
		
//		JScrollPane pane = new JScrollPane(table);
//        panel.add(pane, BorderLayout.CENTER);
	}
}
