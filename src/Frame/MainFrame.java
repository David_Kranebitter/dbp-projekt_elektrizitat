package Frame;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartPanel;

import DB.ChartPrinter;
import DB.DBManager;
import Models.Country;
import java.awt.Font;

public class MainFrame extends JFrame {

	private static MainFrame instance = null;
	public String selection = "";

	private JPanel contentPane;
	private RangeFrame range;
	private ExcludeNationsFrame eN;
	private ShowData sD;
	public JButton btnDraw;

	private boolean isBarChart = false;
	private boolean isLineChart = false;
	private boolean isPieChart = false;

	private String[] list;
	private ArrayList<Country> selectedCountryList;

	/**
	 * Launch the application.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */

	public static MainFrame getInstance() throws ClassNotFoundException, SQLException {
		if (instance == null) {
			instance = new MainFrame();
		}
		return instance;
	}

	/**
	 * Create the frame.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public MainFrame() throws ClassNotFoundException, SQLException {
		setResizable(false);
		setTitle("ChartDrawer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 853, 751);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 831, 695);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton btnBarchart = new JButton("Barchart");
		JButton btnPiechart = new JButton("Piechart");
		JButton btnNewButton = new JButton("Linechart");
		btnBarchart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isLineChart = false;
				isBarChart = true;
				isPieChart = false;

				btnBarchart.setBackground(Color.GREEN);
				btnPiechart.setBackground(Color.RED);
				btnNewButton.setBackground(Color.RED);

				btnDraw.doClick();
			}
		});
		btnBarchart.setBounds(701, 16, 115, 29);
		panel.add(btnBarchart);

		btnPiechart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isLineChart = false;
				isBarChart = false;
				isPieChart = true;

				btnBarchart.setBackground(Color.RED);
				btnPiechart.setBackground(Color.GREEN);
				btnNewButton.setBackground(Color.RED);

				btnDraw.doClick();
			}
		});
		btnPiechart.setBounds(571, 16, 115, 29);
		panel.add(btnPiechart);

		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				isLineChart = true;
				isBarChart = false;
				isPieChart = false;

				btnBarchart.setBackground(Color.RED);
				btnPiechart.setBackground(Color.RED);
				btnNewButton.setBackground(Color.GREEN);

				btnDraw.doClick();
			}
		});
		btnNewButton.setBounds(441, 16, 115, 29);
		panel.add(btnNewButton);

		JSeparator separator = new JSeparator();
		separator.setBounds(15, 61, 801, 2);
		panel.add(separator);

		JButton btnNewButton_1 = new JButton("Compare Nations");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					eN = new ExcludeNationsFrame();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				eN.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(15, 79, 164, 29);
		panel.add(btnNewButton_1);

		JButton btnApplyRange = new JButton("Time period");
		btnApplyRange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				range = new RangeFrame();
				range.setVisible(true);
			}
		});
		btnApplyRange.setBounds(194, 79, 132, 29);
		panel.add(btnApplyRange);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(15, 124, 801, 2);
		panel.add(separator_1);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.GRAY);
		panel_1.setBounds(15, 142, 801, 537);
		panel.add(panel_1);

		JLabel lblMin = new JLabel("MIN: ");
		lblMin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMin.setBounds(15, 22, 96, 16);
		panel.add(lblMin);
		
		JLabel lblMax = new JLabel("MAX: ");
		lblMax.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMax.setBounds(123, 22, 102, 16);
		panel.add(lblMax);
		
		JLabel lblAvg = new JLabel("AVG: ");
		lblAvg.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAvg.setBounds(248, 22, 124, 16);
		panel.add(lblAvg);

		list = DBManager.getInstance().getCountries();

		JComboBox<String> comboBox = new JComboBox<String>(list);// list
		comboBox.setBounds(436, 79, 250, 29);
		panel.add(comboBox);

		btnDraw = new JButton("Draw");
		btnDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					selection = (String) comboBox.getSelectedItem();
					ArrayList<Country>[] lists = null;
					if (!selection.equals("None")) {
						selectedCountryList = DBManager.getInstance().getCountryData(selection);
						String[] sArr = new String[4];
						lists = ChartPrinter.getInstance().getLists();
						for (int i = 1; i < lists.length; i++) {
							// sArr[i] = lists[i].get(0).getCountryName();
							if (lists[i] != null) {
								lists[i] = DBManager.getInstance().getCountryData(lists[i].get(0).getCountryName());
							}
						}

						// lists = ChartPrinter.getInstance().countryLists;
						lists[0] = selectedCountryList;
						ChartPrinter.getInstance().setList(lists);
					} else {
						ChartPrinter.getInstance().clearFirst();
						ChartPrinter.getInstance().setList(lists);
					}
					ChartPanel cPanel = null;
					if (isBarChart && !isLineChart && !isPieChart) {
						cPanel = ChartPrinter.getInstance().printBarChart();
					}
					if (!isBarChart && isLineChart && !isPieChart) {
						cPanel = ChartPrinter.getInstance().printLineChart();
					}
					if (!isBarChart && !isLineChart && isPieChart) {
						cPanel = ChartPrinter.getInstance().printPieChart();
					}
					int[] information = DBManager.getInstance().getInformation(selection);
					lblMin.setText("Min: " + information[0]);
					lblMax.setText("Max: " + information[1]);
					lblAvg.setText("Avg: " + information[2]);
					cPanel.setBounds(100, 142, 1000, 800);
					panel_1.removeAll();
					panel_1.add(cPanel);
					panel.add(panel_1);
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnDraw.setBounds(701, 79, 115, 29);
		panel.add(btnDraw);
		
		JButton btnNewButton_2 = new JButton("Sow");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(selectedCountryList.size());
				sD = new ShowData(selectedCountryList);
				sD.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(338, 79, 86, 29);
		panel.add(btnNewButton_2);
	}
}
