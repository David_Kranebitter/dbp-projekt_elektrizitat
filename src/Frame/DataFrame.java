package Frame;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JRadioButton;

import DB.DBManager;

import DB.*;

public class DataFrame {

	private JFrame frmDateipfadAuswhlen;
	private MainFrame mf;
	private JTextField textField;
	private JTextField textField_1;
	private String inputPath = "";

	private boolean isLocalHost = true;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataFrame window = new DataFrame();
					window.frmDateipfadAuswhlen.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DataFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initialize() {
		frmDateipfadAuswhlen = new JFrame();
		frmDateipfadAuswhlen.setTitle("Dateipfad ausw\u00E4hlen");
		frmDateipfadAuswhlen.setBounds(100, 100, 720, 358);
		frmDateipfadAuswhlen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDateipfadAuswhlen.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 698, 311);
		panel.setBackground(Color.WHITE);
		frmDateipfadAuswhlen.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblDateiAuswhlen = new JLabel("Datei ausw\u00E4hlen: ");
		lblDateiAuswhlen.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDateiAuswhlen.setBounds(59, 26, 126, 20);
		panel.add(lblDateiAuswhlen);

		textField = new JTextField();
		textField.setBounds(200, 23, 275, 26);
		panel.add(textField);
		textField.setColumns(10);

		JButton btnDurchsuchen = new JButton("Durchsuchen");
		btnDurchsuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new java.io.File("user.dir"));
				fc.setDialogTitle("Durchsuchen");
				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fc.setAcceptAllFileFilterUsed(false);

				if (fc.showOpenDialog(btnDurchsuchen) == JFileChooser.APPROVE_OPTION) {
					inputPath = fc.getSelectedFile().getAbsolutePath();
					System.out.println(inputPath);
					textField.setText(inputPath);
				}
			}
		});
		btnDurchsuchen.setBounds(506, 22, 139, 29);
		panel.add(btnDurchsuchen);

		JSeparator separator = new JSeparator();
		separator.setBounds(25, 62, 644, 2);
		panel.add(separator);

		JLabel lblIpadresse = new JLabel("IP-Adresse:");
		lblIpadresse.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIpadresse.setBounds(59, 80, 92, 20);
		panel.add(lblIpadresse);

		textField_1 = new JTextField();
		textField_1.setText("192.168.43.198");
		textField_1.setColumns(10);
		textField_1.setBounds(166, 77, 146, 26);
		panel.add(textField_1);

		JRadioButton radioButton = new JRadioButton("");
		radioButton.setBackground(Color.WHITE);
		radioButton.setBounds(11, 22, 29, 29);
		radioButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isLocalHost = true;
				textField.setEditable(true);
				btnDurchsuchen.setEnabled(true);
				textField_1.setEditable(false);
				textField_2.setEditable(false);
				textField_3.setEditable(false);
				textField_4.setEditable(false);
				textField_5.setEditable(false);
			}
		});
		panel.add(radioButton);

		JRadioButton radioButton_1 = new JRadioButton("");
		radioButton_1.setBackground(Color.WHITE);
		radioButton_1.setBounds(11, 76, 29, 29);
		radioButton_1.setEnabled(true);
		radioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isLocalHost = false;
				textField_1.setEditable(true);
				textField_2.setEditable(true);
				textField_3.setEditable(true);
				textField_4.setEditable(true);
				textField_5.setEditable(true);
				textField.setEditable(false);
				btnDurchsuchen.setEnabled(false);
			}

		});
		panel.add(radioButton_1);

		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				String path = textField.getText();
//				path.replaceAll("\\", "\\\\");
				ReadFile rd = new ReadFile("C:\\Users\\David\\Desktop\\dbp-Projekt\\dbp-projekt_elektrizitat\\src\\nrg_105m_1_Data.csv");
				if (isLocalHost && textField_1.getText().equals("") && !textField.getText().equals("")) {
					try {
						rd.readCSV();
					} catch (Exception e1) {
						e1.printStackTrace();
					}

				}

				if (!isLocalHost && textField.getText().equals("") && !textField_1.getText().equals("")) {
					try {
						DBManager.getInstance().setConData(textField_3.getText(), textField_4.getText(),
								textField_5.getText(), textField_1.getText(), textField_2.getText());
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				try {
					mf = MainFrame.getInstance();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				mf.setVisible(true);
				frmDateipfadAuswhlen.dispose();
			}
		});
		btnNewButton.setBounds(571, 274, 115, 29);
		panel.add(btnNewButton);

		JLabel lblNewLabel = new JLabel("Port:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(59, 116, 69, 20);
		panel.add(lblNewLabel);

		textField_2 = new JTextField();
		textField_2.setText("3306");
		textField_2.setColumns(10);
		textField_2.setBounds(166, 113, 146, 26);
		panel.add(textField_2);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblUsername.setBounds(59, 154, 92, 20);
		panel.add(lblUsername);

		textField_3 = new JTextField();
		textField_3.setText("remoteuser");
		textField_3.setColumns(10);
		textField_3.setBounds(166, 151, 146, 26);
		panel.add(textField_3);

		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPasswort.setBounds(59, 190, 92, 20);
		panel.add(lblPasswort);

		textField_4 = new JTextField();
		textField_4.setText("remoteUser");
		textField_4.setColumns(10);
		textField_4.setBounds(166, 187, 146, 26);
		panel.add(textField_4);

		JLabel lblDatenbankname = new JLabel("Datenbank:");
		lblDatenbankname.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDatenbankname.setBounds(59, 226, 92, 20);
		panel.add(lblDatenbankname);

		textField_5 = new JTextField();
		textField_5.setText("projectelectric");
		textField_5.setColumns(10);
		textField_5.setBounds(166, 223, 146, 26);
		panel.add(textField_5);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(25, 259, 644, 2);
		panel.add(separator_1);

		JRadioButton radioButton_2 = new JRadioButton("");
		radioButton_2.setEnabled(true);
		radioButton_2.setBackground(Color.WHITE);
		radioButton_2.setBounds(11, 271, 29, 29);
		radioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isLocalHost = true;
				textField_1.setEditable(false);
				textField_2.setEditable(false);
				textField_3.setEditable(false);
				textField_4.setEditable(false);
				textField_5.setEditable(false);
				textField.setEditable(false);
				btnDurchsuchen.setEnabled(false);
			}

		});
		panel.add(radioButton_2);

		ButtonGroup bg = new ButtonGroup();
		bg.add(radioButton);
		bg.add(radioButton_1);
		bg.add(radioButton_2);

		JLabel lblMitBestehendenDaten = new JLabel("Mit bestehenden Daten fortfahren");
		lblMitBestehendenDaten.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMitBestehendenDaten.setBounds(59, 274, 253, 20);
		panel.add(lblMitBestehendenDaten);
	}
}
