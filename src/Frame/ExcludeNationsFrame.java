package Frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DB.ChartPrinter;
import DB.DBManager;
import Models.Country;

public class ExcludeNationsFrame extends JFrame {
	
	private static ExcludeNationsFrame instance = null;

	private JPanel contentPane;
	private ArrayList<Country>[] cLists = new ArrayList[5];

	public JButton btnAktualisieren;
	
	public static ExcludeNationsFrame getInstance() throws ClassNotFoundException, SQLException {
		if(instance == null) {
			instance = new ExcludeNationsFrame();
		}
		return instance;
	}
	
	/**
	 * Launch the application.
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExcludeNationsFrame frame = new ExcludeNationsFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public ExcludeNationsFrame() throws ClassNotFoundException, SQLException {
		setTitle("Compare");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 224);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 428, 177);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblRot = new JLabel("Violett:");
		lblRot.setBounds(12, 13, 56, 16);
		panel.add(lblRot);
		
		JLabel lblGrn = new JLabel("Gr\u00FCn:");
		lblGrn.setBounds(12, 52, 56, 16);
		panel.add(lblGrn);
		
		JLabel lblBlau = new JLabel("Blau");
		lblBlau.setBounds(12, 93, 56, 16);
		panel.add(lblBlau);
		
		JLabel lblSchwarz = new JLabel("Schwarz:");
		lblSchwarz.setBounds(12, 135, 56, 16);
		panel.add(lblSchwarz);
		
		String[] list = DBManager.getInstance().getCountries();
		
		JComboBox<String> comboBox = new JComboBox<String>(list);
		comboBox.setBounds(111, 13, 127, 22);
		panel.add(comboBox);
		
		JComboBox<String> comboBox_1 = new JComboBox<String>(list);
		comboBox_1.setBounds(111, 49, 127, 22);
		panel.add(comboBox_1);
		
		JComboBox<String> comboBox_2 = new JComboBox<String>(list);
		comboBox_2.setBounds(111, 90, 127, 22);
		panel.add(comboBox_2);
		
		JComboBox<String> comboBox_3 = new JComboBox<String>(list);
		comboBox_3.setBounds(111, 132, 127, 22);
		panel.add(comboBox_3);
		
		btnAktualisieren = new JButton("Aktualisieren");
		btnAktualisieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] selections = {(String) comboBox.getSelectedItem(), (String) comboBox_1.getSelectedItem(), (String) comboBox_2.getSelectedItem(), (String) comboBox_3.getSelectedItem()};
				try {
					if(!comboBox.getSelectedItem().equals("None")) {
						cLists[1] = DBManager.getInstance().getCountryData(selections[0]);
					}else {
						cLists[1] = null;
					}
					if(!comboBox_1.getSelectedItem().equals("None")) {
						cLists[2] = DBManager.getInstance().getCountryData(selections[1]);
					}else {
						cLists[2] = null;
					}
					if(!comboBox_2.getSelectedItem().equals("None")) {
						cLists[3] = DBManager.getInstance().getCountryData(selections[2]);
					}else {
						cLists[3] = null;
					}
					if(!comboBox_3.getSelectedItem().equals("None")) {
						cLists[4] = DBManager.getInstance().getCountryData(selections[3]);
					}else {
						cLists[4] = null;
					}
					ChartPrinter.getInstance().setList(cLists);
					
//					for(ArrayList<Country> cList : cLists) {
//						ChartPrinter.getInstance().addList(cList);
//					}
					MainFrame.getInstance().btnDraw.doClick();
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnAktualisieren.setBounds(285, 68, 117, 25);
		panel.add(btnAktualisieren);
		
		JButton btnNewButton = new JButton("Reset");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cLists[0] = null;
				cLists[1] = null;
				cLists[2] = null;
				cLists[3] = null;
				cLists[4] = null;
				ChartPrinter.getInstance().setList(cLists);
				try {
					MainFrame.getInstance().btnDraw.doClick();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(285, 106, 117, 25);
		panel.add(btnNewButton);
	}
}
