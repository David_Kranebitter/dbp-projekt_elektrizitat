package Frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdatepicker.JDatePicker;

import DB.DBManager;

public class RangeFrame extends JFrame {

	private JPanel contentPane;
	private java.sql.Date minDate;
	private java.sql.Date maxDate;

	private JDatePicker picker2;
	private JDatePicker picker;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RangeFrame frame = new RangeFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RangeFrame() {
		setTitle("Zeitspanne");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 429, 134);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 411, 87);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblStartdatum = new JLabel("Startdatum:");
		lblStartdatum.setBounds(15, 16, 94, 20);
		panel.add(lblStartdatum);

		JLabel lblEnddatum = new JLabel("Enddatum:");
		lblEnddatum.setBounds(15, 48, 83, 20);
		panel.add(lblEnddatum);

		picker = new JDatePicker();
		picker.setTextEditable(true);
		picker.setShowYearButtons(true);
		picker.setBounds(95, 16, 150, 25);

		picker.addActionListener(e -> {
			Calendar selectedValue = (Calendar) picker.getModel().getValue();
			minDate = new java.sql.Date(selectedValue.getTime().getTime());
			System.out.println(minDate);
		});
		panel.add(picker);

		picker2 = new JDatePicker();
		picker2.setTextEditable(true);
		picker2.setShowYearButtons(true);
		picker2.setBounds(95, 49, 150, 25);

		picker2.addActionListener(e -> {
			Calendar selectedValue = (Calendar) picker2.getModel().getValue();
			maxDate = new java.sql.Date(selectedValue.getTime().getTime());
			System.out.println(maxDate);
		});
		panel.add(picker2);

		JButton btnNewButton = new JButton("Apply");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Calendar min = (Calendar) picker.getModel().getValue();
					minDate = new java.sql.Date(min.getTime().getTime());
					Calendar max = (Calendar) picker2.getModel().getValue();
					maxDate = new java.sql.Date(max.getTime().getTime());
					DBManager.getInstance().setDates(minDate, maxDate);
					if (ExcludeNationsFrame.getInstance().btnAktualisieren != null) {
						ExcludeNationsFrame.getInstance().btnAktualisieren.doClick();
						System.out.println("pressed");
					}
					MainFrame.getInstance().btnDraw.doClick();
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(290, 14, 97, 25);
		panel.add(btnNewButton);

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
					minDate = new java.sql.Date(sdf.parse("2018-01").getTime());
					maxDate = new java.sql.Date(sdf.parse("2019-01").getTime());
					DBManager.getInstance().setDates(minDate, maxDate);
					ExcludeNationsFrame.getInstance().btnAktualisieren.doClick();
					MainFrame.getInstance().btnDraw.doClick();
				} catch (ClassNotFoundException | SQLException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnReset.setBounds(290, 46, 97, 25);
		panel.add(btnReset);
	}
}
