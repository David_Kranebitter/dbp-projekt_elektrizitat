package DB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;
import Models.*;

public class ReadFile {

	String filepath;

	public ReadFile(String filepath) {
		this.filepath = filepath;
	}

	public void readCSV() throws IOException, FileNotFoundException, NumberFormatException, ParseException,
			ClassNotFoundException, SQLException {
		FileReader fr = new FileReader(filepath);
		BufferedReader br = new BufferedReader(fr);

		ArrayList<String> lines = new ArrayList<String>();
		br.readLine();
		String line = br.readLine();

		/*
		 * Alle Werte, die die Europ�ische Union betreffen werden von der While-Schleife
		 * verworfen. Ebenso wird nach der gew�nschten Einf�gung selektiert und alle
		 * anderen Eintr�ge in der CSV-Datei verworfen. Sollte die Eingelesene Zeile
		 * nicht mehr existieren, wird die Schleife abgebrochen.
		 */

		while (line != null) {
			
			if(line.contains("Euro")) {
				line = br.readLine();
				continue; 
			}

			line = line.replaceAll("\"", "");

			String country = null;
			String month = null;
			String value = "";

			StringTokenizer lineedit = new StringTokenizer(line, ","); // die eingelesene Zeile wird auf "," �berpr�ft
																		// und dort getrennt

			// tempor�re Speicherung der Werte
			month = lineedit.nextToken();
			country = lineedit.nextToken();

			for (int i = 0; i < 3; i++) {
				lineedit.nextToken();
			}

			value = lineedit.nextToken();

			// Wenn kein Wert angegeben ist, wird in der Datei das Sonderzeichen ":"
			// verwendet. Dieses wird hier abgefangen
			if (value.contains(":")) {
				/*
				 * Da die Zahlenwerte auch ein "," besitzen, um die Trennung von Ganzzahl und
				 * Kommastelle anzuzeigen wird hier dieser Wert mit "." verbunden
				 */
				// value = value + "." + lineedit.nextToken();
				// die Konvertierung in Double ist mit der Tausenderstellentrennung "." nicht
				// m�glich, deshalb wird hier der "." entfernt
				value = "0.0";
				System.out.println(value);
//				continue;
			}

			month = month.replaceAll("M", "-");
			if (country.contains("(")) {
				country = country.substring(0, country.indexOf('(') - 1);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

			Country c = null;
			try {
				c = new Country(country, Double.parseDouble(value), sdf.parse(month));
			} catch (Exception e) {
				System.out.println("Wrong Format");
			}

			// lines.add(country);
			// lines.add(month);
			// lines.add(value);
			if (c != null) {
				DBManager.getInstance().insertData(c);

			}

			line = br.readLine();

		}
		fr.close();
		br.close();
	}

	private String changeValue(String value) {
		if (value.isEmpty()) {
			return "0.0";
		}

		value = value.substring(0, value.indexOf(".")) + value.substring(value.indexOf(".") + 1, value.length());

		return value;
	}

	// private void insertDatabase(String table, ArrayList<String> dates) {
	// ArrayList<Double> belgium = new ArrayList<Double>();
	// ArrayList<Double> bulgaria = new ArrayList<Double>();
	// ArrayList<Double> czech = new ArrayList<Double>();
	// ArrayList<Double> denmark = new ArrayList<Double>();
	// ArrayList<Double> germany = new ArrayList<Double>();
	// ArrayList<Double> estonia = new ArrayList<Double>();
	// ArrayList<Double> ireland = new ArrayList<Double>();
	// ArrayList<Double> greece = new ArrayList<Double>();
	// ArrayList<Double> spain = new ArrayList<Double>();
	// ArrayList<Double> france = new ArrayList<Double>();
	// ArrayList<Double> croatia = new ArrayList<Double>();
	// ArrayList<Double> italy = new ArrayList<Double>();
	// ArrayList<Double> zypress = new ArrayList<Double>();
	// ArrayList<Double> latvia = new ArrayList<Double>();
	// ArrayList<Double> lithuania = new ArrayList<Double>();
	// ArrayList<Double> luxembourg = new ArrayList<Double>();
	// ArrayList<Double> hungary = new ArrayList<Double>();
	// ArrayList<Double> malta = new ArrayList<Double>();
	// ArrayList<Double> netherlands = new ArrayList<Double>();
	// ArrayList<Double> austria = new ArrayList<Double>();
	// ArrayList<Double> poland = new ArrayList<Double>();
	// ArrayList<Double> portugal = new ArrayList<Double>();
	// ArrayList<Double> romania = new ArrayList<Double>();
	// ArrayList<Double> slowenia = new ArrayList<Double>();
	// ArrayList<Double> slovakia = new ArrayList<Double>();
	// ArrayList<Double> finnland = new ArrayList<Double>();
	// ArrayList<Double> sweden = new ArrayList<Double>();
	// ArrayList<Double> uk = new ArrayList<Double>();
	// ArrayList<Double> iceland = new ArrayList<Double>();
	// ArrayList<Double> norway = new ArrayList<Double>();
	// ArrayList<Double> montenegro = new ArrayList<Double>();
	// ArrayList<Double> macedonia = new ArrayList<Double>();
	// ArrayList<Double> albania = new ArrayList<Double>();
	// ArrayList<Double> serbia = new ArrayList<Double>();
	// ArrayList<Double> turkey = new ArrayList<Double>();
	// ArrayList<Double> bosnia = new ArrayList<Double>();
	// ArrayList<Double> ukraine = new ArrayList<Double>();
	// ArrayList<Double> georgia = new ArrayList<Double>();
	//
	// for (int i = 0; i < dates.size(); i = i + 3) {
	// switch (dates.get(i)) {
	// case "Belgien":
	// belgium = insertDates(belgium, dates.get(i + 2));
	// break;
	// case "Bulgarien":
	// bulgaria = insertDates(bulgaria, dates.get(i + 2));
	// break;
	// case "Tschechien":
	// czech = insertDates(czech, dates.get(i + 2));
	// break;
	// case "D�nemark":
	// denmark = insertDates(denmark, dates.get(i + 2));
	// break;
	// case "Deutschland (bis 1990 fr�heres Gebiet der BRD)":
	// germany = insertDates(germany, dates.get(i + 2));
	// break;
	// case "Estland":
	// estonia = insertDates(estonia, dates.get(i + 2));
	// break;
	// case "Griechenland":
	// greece = insertDates(greece, dates.get(i + 2));
	// break;
	// case "Spanien":
	// spain = insertDates(spain, dates.get(i + 2));
	// break;
	// case "Irland":
	// ireland = insertDates(ireland, dates.get(i + 2));
	// break;
	// case "Frankreich":
	// france = insertDates(france, dates.get(i + 2));
	// break;
	// case "Kroatien":
	// croatia = insertDates(croatia, dates.get(i + 2));
	// break;
	// case "Italien":
	// italy = insertDates(italy, dates.get(i + 2));
	// break;
	// case "Zypern":
	// zypress = insertDates(zypress, dates.get(i + 2));
	// break;
	// case "Lettland":
	// latvia = insertDates(latvia, dates.get(i + 2));
	// break;
	// case "Litauen":
	// lithuania = insertDates(lithuania, dates.get(i + 2));
	// break;
	// case "Luxemburg":
	// luxembourg = insertDates(luxembourg, dates.get(i + 2));
	// break;
	// case "Ungarn":
	// hungary = insertDates(hungary, dates.get(i + 2));
	// break;
	// case "Malta":
	// malta = insertDates(malta, dates.get(i + 2));
	// break;
	// case "Niederlande":
	// netherlands = insertDates(netherlands, dates.get(i + 2));
	// break;
	// case "�sterreich":
	// austria = insertDates(austria, dates.get(i + 2));
	// break;
	// case "Polen":
	// poland = insertDates(poland, dates.get(i + 2));
	// break;
	// case "Portugal":
	// portugal = insertDates(portugal, dates.get(i + 2));
	// break;
	// case "Rum�nien":
	// romania = insertDates(romania, dates.get(i + 2));
	// break;
	// case "Slowenien":
	// slowenia = insertDates(slowenia, dates.get(i + 2));
	// break;
	// case "Slowakei":
	// slovakia = insertDates(slovakia, dates.get(i + 2));
	// break;
	// case "Finnland":
	// finnland = insertDates(finnland, dates.get(i + 2));
	// break;
	// case "Schweden":
	// sweden = insertDates(sweden, dates.get(i + 2));
	// break;
	// case "Vereinigtes K�nigreich":
	// uk = insertDates(uk, dates.get(i + 2));
	// break;
	// case "Island":
	// iceland = insertDates(iceland, dates.get(i + 2));
	// break;
	// case "Norwegen":
	// norway = insertDates(norway, dates.get(i + 2));
	// break;
	// case "Montenegro":
	// montenegro = insertDates(montenegro, dates.get(i + 2));
	// break;
	// case "Nordmazedonien":
	// macedonia = insertDates(macedonia, dates.get(i + 2));
	// break;
	// case "Albanien":
	// albania = insertDates(albania, dates.get(i + 2));
	// break;
	// case "Serbien":
	// serbia = insertDates(serbia, dates.get(i + 2));
	// break;
	// case "T�rkei":
	// turkey = insertDates(turkey, dates.get(i + 2));
	// break;
	// case "Bosnien und Herzegowina":
	// bosnia = insertDates(bosnia, dates.get(i + 2));
	// break;
	// case "Ukraine":
	// ukraine = insertDates(ukraine, dates.get(i + 2));
	// break;
	// case "Georgien":
	// georgia = insertDates(georgia, dates.get(i + 2));
	// break;
	// }
	// }
	//
	// try {
	// DBManager.getInstance().insertDatabase(table, "Belgien", belgium);
	// DBManager.getInstance().insertDatabase(table, "Bulgarien", bulgaria);
	// DBManager.getInstance().insertDatabase(table, "Tschechien", czech);
	// DBManager.getInstance().insertDatabase(table, "D�nemark", denmark);
	// DBManager.getInstance().insertDatabase(table, "Deutschland", germany);
	// DBManager.getInstance().insertDatabase(table, "Estland", estonia);
	// DBManager.getInstance().insertDatabase(table, "Griechenland", greece);
	// DBManager.getInstance().insertDatabase(table, "Spanien", spain);
	// DBManager.getInstance().insertDatabase(table, "Irland", ireland);
	// DBManager.getInstance().insertDatabase(table, "Frankreich", france);
	// DBManager.getInstance().insertDatabase(table, "Kroatien", croatia);
	// DBManager.getInstance().insertDatabase(table, "Italien", italy);
	// DBManager.getInstance().insertDatabase(table, "Zypern", zypress);
	// DBManager.getInstance().insertDatabase(table, "Lettland", latvia);
	// DBManager.getInstance().insertDatabase(table, "Litauen", lithuania);
	// DBManager.getInstance().insertDatabase(table, "Luxemburg", luxembourg);
	// DBManager.getInstance().insertDatabase(table, "Ungarn", hungary);
	// DBManager.getInstance().insertDatabase(table, "Malta", malta);
	// DBManager.getInstance().insertDatabase(table, "Niederlande", netherlands);
	// DBManager.getInstance().insertDatabase(table, "�sterreich", austria);
	// DBManager.getInstance().insertDatabase(table, "Polen", poland);
	// DBManager.getInstance().insertDatabase(table, "Portugal", portugal);
	// DBManager.getInstance().insertDatabase(table, "Rum�nien", romania);
	// DBManager.getInstance().insertDatabase(table, "Slowenien", slowenia);
	// DBManager.getInstance().insertDatabase(table, "Slowakei", slovakia);
	// DBManager.getInstance().insertDatabase(table, "Finnland", finnland);
	// DBManager.getInstance().insertDatabase(table, "Schweden", sweden);
	// DBManager.getInstance().insertDatabase(table, "Vereinigtes K�nigreich", uk);
	// DBManager.getInstance().insertDatabase(table, "Island", iceland);
	// DBManager.getInstance().insertDatabase(table, "Norwegen", norway);
	// DBManager.getInstance().insertDatabase(table, "Montenegro", montenegro);
	// DBManager.getInstance().insertDatabase(table, "Mazedonien", macedonia);
	// DBManager.getInstance().insertDatabase(table, "Albanien", albania);
	// DBManager.getInstance().insertDatabase(table, "Serbien", serbia);
	// DBManager.getInstance().insertDatabase(table, "T�rkei", turkey);
	// DBManager.getInstance().insertDatabase(table, "Bosnien und Herzegowina",
	// bosnia);
	// DBManager.getInstance().insertDatabase(table, "Ukraine", ukraine);
	// DBManager.getInstance().insertDatabase(table, "Georgien", georgia);
	// } catch (ClassNotFoundException | SQLException e) {
	// e.printStackTrace();
	// }
	// }

	public static ArrayList<Double> insertDates(ArrayList<Double> list, String value) {
		try {
			list.add(Double.parseDouble(value));
		} catch (Exception e) {
			list.add(0.0);
		}

		return list;
	}
}
