package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import Models.Country;

public class DBManager {
	private static DBManager instance = null;
	private String user;
	private String pw;
	private String db;
	private String ip;
	private String port;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
	private java.sql.Date minDate;
	private java.sql.Date maxDate;

	public void setDates(java.sql.Date minDate, java.sql.Date maxDate) {
		this.minDate = minDate;
		this.maxDate = maxDate;
	}

	private DBManager() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		try {
			setDates(new java.sql.Date(sdf.parse("2017-01").getTime()),
					new java.sql.Date(sdf.parse("2019-01").getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// InputStreamReader isr = new InputStreamReader(System.in);
		// BufferedReader br = new BufferedReader(isr);
		//
		// Anmeldung bei der Datenbank
		// try
		// {
		// System.out.print("MySQL Username: ");
		// user = br.readLine();
		// System.out.println();
		//
		// System.out.print("MySQL Password: ");
		// pw = br.readLine();
		// System.out.println();
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	public void setConData(String user, String pw, String db, String ip, String port) {
		this.user = user;
		this.pw = pw;
		this.db = db;
		this.ip = ip;
		this.port = port;
	}

	public static DBManager getInstance() throws ClassNotFoundException {
		if (instance == null) {
			instance = new DBManager();
		}

		return instance;
	}

	// Aufbauen der Connection zur Datenbank
	private Connection openConnection() throws ClassNotFoundException, SQLException {
		Connection c;

//		pw = "Iquoxum321";
//		db = "elektro";

		Class.forName("com.mysql.jdbc.Driver");
		System.out.println(ip);
		if(ip == null) {
			pw = "Iquoxum321";
			db = "elektro";
			c = DriverManager.getConnection("jdbc:mysql://localhost/" + db, "root", pw);
			System.out.println("Local");
		}else {
			c = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + db, user, pw);
			System.out.println("Remote");
		}

		return c;
	}

	// Verbindung beenden, damit diese Instanz nicht unentwegt verwendet wird
	private void refuseConnection(Connection c) throws SQLException {
		c.close();
	}

	public void insertData(Country country) throws ClassNotFoundException, SQLException {
		Connection con = openConnection();
		PreparedStatement prstmt = null;

		try {
			String sql = "INSERT INTO countryData(TIME, GEO, VALUE) VALUES(?,?,?);";

			prstmt = con.prepareStatement(sql);

			prstmt.setDate(1, new java.sql.Date(country.getDate().getTime()));
			prstmt.setString(2, country.getCountryName());
			prstmt.setDouble(3, country.getValue());

			prstmt.executeUpdate();
		} finally {
			if (prstmt != null) {
				prstmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}

	}

	public void createDataTable() throws ClassNotFoundException, SQLException {
		Connection con = openConnection();
		PreparedStatement prstmt = null;

		try {
			String sql = "CREATE TABLE IF NOT EXISTS data (country VARCHAR(255) PRIMARY KEY, 2018M03 double, 2018M04 double, 2018M05 double, 2018M06 double"
					+ ", 2018M07 double, 2018M08 double, 2018M09 double, 2018M10 double, 2018M11 double, 2018M12 double);";

			prstmt = con.prepareStatement(sql);
			prstmt.executeUpdate();
		} finally {
			if (prstmt != null) {
				prstmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}
	}

	public int[] getInformation(String country) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int[] information = new int[5];
		String sql[] = { "SELECT MIN(VALUE) FROM countryData WHERE GEO = ?;",
				"SELECT MAX(VALUE) FROM countryData WHERE GEO = ?;",
				"SELECT AVG(VALUE) FROM countryData WHERE GEO = ?;" };

		try {
			con = openConnection();
			for (int i = 0; i < sql.length; i++) {
				stmt = con.prepareStatement(sql[i]);
				stmt.setString(1, country);
				rs = stmt.executeQuery();
				while (rs.next()) {
					information[i] = rs.getInt(1);
					System.out.println(rs.getInt(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}

		return information;
	}

	public ArrayList<Country> getCountryData(String country) throws SQLException {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<Country> list = new ArrayList<Country>();

		String sql = "SELECT TIME, GEO, VALUE FROM countryData WHERE GEO = ? && unix_timestamp(TIME) > ? && unix_timestamp(TIME) < ?";
		try {
			con = openConnection();
			stmt = con.prepareStatement(sql);
			stmt.setString(1, country);
			stmt.setLong(2, minDate.getTime() / 1000);
			stmt.setLong(3, maxDate.getTime() / 1000);
			rs = stmt.executeQuery();
			while (rs.next()) {
				list.add(new Country(rs.getString(2), rs.getDouble(3), rs.getDate(1)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}
		return list;
	}

	// Einzelner Output eines gewissen Monatswertes eines angegebenen Landes
	public double getData(String country, int month) throws SQLException, ClassNotFoundException {
		Connection con = openConnection();
		PreparedStatement prstmt = null;
		ResultSet rs = null;

		String monthstring = "";

		try {
			switch (month) {
			case 3:
				monthstring = "2018M03";
				break;
			case 4:
				monthstring = "2018M04";
				break;
			case 5:
				monthstring = "2018M05";
				break;
			case 6:
				monthstring = "2018M06";
				break;
			case 7:
				monthstring = "2018M07";
				break;
			case 8:
				monthstring = "2018M08";
				break;
			case 9:
				monthstring = "2018M09";
				break;
			case 10:
				monthstring = "2018M10";
				break;
			case 11:
				monthstring = "2018M11";
				break;
			case 12:
				monthstring = "2018M12";
				break;
			default:
				System.err.println("Invalid Input! Select a month from March (3) to December (12), please!");
				return -1.0;
			}
			String sql = "SELECT " + monthstring + " from data WHERE country = " + country + ";";

			prstmt = con.prepareStatement(sql);

			rs = prstmt.executeQuery();

			return rs.getDouble(1);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (prstmt != null) {
				prstmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}

	}

	// public ArrayList<Double> getCountryData(String country) throws
	// ClassNotFoundException, SQLException
	// {
	// ArrayList<Double> countryData = new ArrayList<Double>();
	//
	// for(int i = 3; i < 13; i++)
	// {
	// countryData.add(getData(country, i));
	// }
	//
	// return countryData;
	// }

	public String[] getCountries() throws SQLException, ClassNotFoundException {
		Connection con = openConnection();
		PreparedStatement prstmt = null;
		ResultSet rs = null;

		try {
			String[] countries = new String[61];
			countries[0] = "None";

			String sql = "SELECT GEO FROM countryData GROUP BY GEO HAVING COUNT(*) > 1;";

			prstmt = con.prepareStatement(sql);
			rs = prstmt.executeQuery();

			for (int i = 1; i < 60; i++) {
				if (rs.next()) {
					countries[i] = rs.getString(1);
				}
			}

			return countries;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (prstmt != null) {
				prstmt.close();
			}
			if (con != null) {
				refuseConnection(con);
			}
		}
	}
}
