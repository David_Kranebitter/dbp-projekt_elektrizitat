package DB;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import Models.Country;

public class ChartPrinter {

	private static ChartPrinter instance;
	public ArrayList<Country>[] countryLists = new ArrayList[5];

	public ChartPrinter() {
	}

	public static ChartPrinter getInstance() {
		if (instance == null) {
			instance = new ChartPrinter();
		}
		return instance;
	}

	public void setList(ArrayList<Country>[] list) {
		this.countryLists = list;
	}

	public ArrayList<Country>[] getLists() {
		return this.countryLists;
	}

	public void addList(ArrayList<Country> list) {
		for (int i = 0; i < countryLists.length; i++) {
			if (countryLists[i] == null) {
				countryLists[i] = list;
				break;
			}
		}
	}

	public void clearFirst() {
		countryLists[0] = null;
	}

	public DefaultCategoryDataset[] fillDataSetArr() {
		DefaultCategoryDataset[] datasetArr = new DefaultCategoryDataset[5];
		DefaultCategoryDataset dataset1 = new DefaultCategoryDataset();
		datasetArr[0] = dataset1;
		DefaultCategoryDataset dataset2 = new DefaultCategoryDataset();
		datasetArr[1] = dataset2;
		DefaultCategoryDataset dataset3 = new DefaultCategoryDataset();
		datasetArr[2] = dataset3;
		DefaultCategoryDataset dataset4 = new DefaultCategoryDataset();
		datasetArr[3] = dataset4;
		DefaultCategoryDataset dataset5 = new DefaultCategoryDataset();
		datasetArr[4] = dataset5;
		return datasetArr;
	}

	public ChartPanel printLineChart() {
		DefaultCategoryDataset[] datasetArr = fillDataSetArr();

		for (int i = 0; i < countryLists.length; i++) {
			if (countryLists[i] == null) {
				continue;
			}
			for (int j = 0; j < countryLists[i].size(); j++) {
				if (datasetArr[i] != null && countryLists != null && countryLists[i] != null) {
					try {
						datasetArr[i].addValue(countryLists[i].get(j).getValue(),
								countryLists[i].get(j).getCountryName(), countryLists[i].get(j).getDate());
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}

		}

		CategoryPlot plot = new CategoryPlot();

		// Add the first dataset and render as bar
		CategoryItemRenderer lineRenderer = new LineAndShapeRenderer();
		lineRenderer.setSeriesPaint(0, Color.PINK);
		plot.setDataset(0, datasetArr[0]);
		plot.setRenderer(0, lineRenderer);

		// Add the second dataset and render as lines
		CategoryItemRenderer lineRenderer1 = new LineAndShapeRenderer();
		lineRenderer1.setSeriesPaint(1, Color.CYAN);
		plot.setDataset(1, datasetArr[1]);
		plot.setRenderer(1, lineRenderer1);

		CategoryItemRenderer lineRenderer2 = new LineAndShapeRenderer();
		lineRenderer2.setSeriesPaint(2, Color.GREEN);
		plot.setDataset(2, datasetArr[2]);
		plot.setRenderer(2, lineRenderer2);

		CategoryItemRenderer lineRenderer3 = new LineAndShapeRenderer();
		lineRenderer3.setSeriesPaint(3, Color.BLUE);
		plot.setDataset(3, datasetArr[3]);
		plot.setRenderer(3, lineRenderer3);

		CategoryItemRenderer lineRenderer4 = new LineAndShapeRenderer();
		lineRenderer4.setSeriesPaint(4, Color.RED);
		plot.setDataset(4, datasetArr[4]);
		plot.setRenderer(4, lineRenderer4);

		// Set Axis
		CategoryAxis cAxis = new CategoryAxis("Datum");
		cAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		Font font = new Font("Dialog", Font.PLAIN, 25);
		Font tickFont = new Font("Dialog", Font.PLAIN, 12);
		cAxis.setLabelFont(font);
		cAxis.setTickLabelFont(tickFont);
		plot.setDomainAxis(cAxis);
		NumberAxis nAxis = new NumberAxis("Gigawattstunde");
		nAxis.setLabelFont(font);
		plot.setRangeAxis(nAxis);
		JFreeChart chart = new JFreeChart(plot);

		ChartPanel chartPanel = new ChartPanel(chart);

		ApplicationFrame app = new ApplicationFrame("Test");

		chartPanel.setPreferredSize(new java.awt.Dimension(801, 537));
		app.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		app.setContentPane(chartPanel);
		app.pack();

		RefineryUtilities.centerFrameOnScreen(app);

		app.setVisible(false);
		return chartPanel;

		// XYSplineRenderer dot = new XYSplineRenderer();
		//
		// NumberAxis xax = new NumberAxis("Zeit");
		//// DateAxis axis = (DateAxis) plot.getDomainAxis();
		//// axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd"));
		// NumberAxis yax = new NumberAxis("Wert");
		//
		//// XYPlot plot = new XYPlot(dataset2, xax, yax, dot);
		//
		// JFreeChart chart2 = new JFreeChart(plot);
	}

	public ChartPanel printBarChart() {
		DefaultCategoryDataset[] datasetArr = fillDataSetArr();

		for (int i = 0; i < countryLists.length; i++) {
			if (countryLists[i] == null) {
				continue;
			}
			for (int j = 0; j < countryLists[i].size(); j++) {
				if (datasetArr[i] != null && countryLists != null && countryLists[i] != null) {
					try {
						datasetArr[i].addValue(countryLists[i].get(j).getValue(),
								countryLists[i].get(j).getCountryName(), countryLists[i].get(j).getDate());
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}

		}

		CategoryPlot plot = new CategoryPlot();

		// Add the first dataset and render as bar
		CategoryItemRenderer baRenderer = new BarRenderer();
		baRenderer.setSeriesPaint(0, Color.GREEN);
		plot.setDataset(0, datasetArr[0]);
		plot.setRenderer(0, baRenderer);

		// Add the second dataset and render as lines
		CategoryItemRenderer baRenderer1 = new BarRenderer();
		baRenderer.setSeriesPaint(1, Color.BLUE);
		plot.setDataset(1, datasetArr[1]);
		plot.setRenderer(1, baRenderer1);

		CategoryItemRenderer baRenderer2 = new BarRenderer();
		baRenderer.setSeriesPaint(2, Color.BLACK);
		plot.setDataset(2, datasetArr[2]);
		plot.setRenderer(2, baRenderer2);

		CategoryItemRenderer baRenderer3 = new BarRenderer();
		baRenderer.setSeriesPaint(3, Color.CYAN);
		plot.setDataset(3, datasetArr[3]);
		plot.setRenderer(3, baRenderer3);

		CategoryItemRenderer baRenderer4 = new BarRenderer();
		baRenderer.setSeriesPaint(4, Color.PINK);
		plot.setDataset(4, datasetArr[4]);
		plot.setRenderer(4, baRenderer4);

		// Set Axis
		CategoryAxis cAxis = new CategoryAxis("Datum");
		cAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		Font font = new Font("Dialog", Font.PLAIN, 25);
		Font tickFont = new Font("Dialog", Font.PLAIN, 12);
		cAxis.setLabelFont(font);
		cAxis.setTickLabelFont(tickFont);
		plot.setDomainAxis(cAxis);
		NumberAxis nAxis = new NumberAxis("Gigawattstunde");
		nAxis.setLabelFont(font);
		LogAxis la = new LogAxis("Gigawattstunde");
		la.setLabelFont(font);
		la.setBase(10);
		la.setSmallestValue(0.1);
		plot.setRangeAxis(la);
		JFreeChart chart = new JFreeChart(plot);

		ChartPanel chartPanel = new ChartPanel(chart);

		ApplicationFrame app = new ApplicationFrame("Test");

		chartPanel.setPreferredSize(new java.awt.Dimension(801, 537));
		app.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		app.setContentPane(chartPanel);
		app.pack();

		RefineryUtilities.centerFrameOnScreen(app);

		app.setVisible(false);
		return chartPanel;

		// XYSplineRenderer dot = new XYSplineRenderer();
		//
		// NumberAxis xax = new NumberAxis("Zeit");
		//// DateAxis axis = (DateAxis) plot.getDomainAxis();
		//// axis.setDateFormatOverride(new SimpleDateFormat("yy/MM/dd"));
		// NumberAxis yax = new NumberAxis("Wert");
		//
		//// XYPlot plot = new XYPlot(dataset2, xax, yax, dot);
		//
		// JFreeChart chart2 = new JFreeChart(plot);
	}

	public ChartPanel printPieChart() {
		DefaultPieDataset dataset = new DefaultPieDataset();

		for(int i = 0; i < countryLists.length; i++) { 
			if(countryLists[i] == null) continue;
			for(int j = 0; j < countryLists[i].size(); j++) {
				dataset.setValue(countryLists[i].get(j).getCountryName(), countryLists[i].get(j).getValue());
			}
		}

		// for (int i = 0; i < countryLists.length; i++) {
		// if (countryLists[i] == null) {
			
		// continue;
		// }
		// for (int j = 0; j < countryLists[i].size(); j++) {
		// if (datasetArr[i] != null && countryLists != null && countryLists[i] != null)
		// {
		// try {
		// datasetArr[i].addValue(countryLists[i].get(j).getValue(),
		// countryLists[i].get(j).getCountryName(), countryLists[i].get(j).getDate());
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// }
		//
		// }
		//
		// }
//		for (Country c : countryLists[0]) {
//			dataset.setValue(c.getCountryName(), c.getValue());
//		}
		CategoryPlot plot = new CategoryPlot();

		JFreeChart chart = ChartFactory.createPieChart("Elektro", // chart title
				dataset, // data
				false, // include legend
				true, false);

		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new java.awt.Dimension(801, 537));

		ApplicationFrame app = new ApplicationFrame("Test");

		app.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		app.setContentPane(chartPanel);
		app.pack();

		RefineryUtilities.centerFrameOnScreen(app);

		// app.setVisible(true);
		return chartPanel;
	}

	// public ChartPanel getLineChartPanel(ArrayList<Double> list, String cName) {
	// XYSeries series1 = new XYSeries("Punkte1");
	// // for(int i = 0; i < list.size(); i++) {
	// // if(list.get(i).getCountryName().equals(cName)) {
	// // for(int j = 0; j < list.get(i).getList().size(); j++) {
	// // series1.add(j, list.get(i).getList().get(j));
	// // }
	// // }
	// // }
	//
	// for (int i = 3; i < list.size(); i++) {
	// series1.add(i, list.get(i));
	// }
	//
	// XYSeriesCollection dataset2 = new XYSeriesCollection();
	// dataset2.addSeries(series1);
	//
	// XYSplineRenderer dot = new XYSplineRenderer();
	//
	// NumberAxis xax = new NumberAxis("x");
	// NumberAxis yax = new NumberAxis("y");
	//
	// XYPlot plot = new XYPlot(dataset2, xax, yax, dot);
	//
	// JFreeChart chart = new JFreeChart(plot);
	//
	// ChartPanel chartPanel = new ChartPanel(chart, false);
	//
	// return chartPanel;
	// }
	//
	//// public ChartPanel getBarChartPanel(Country c) {
	//// DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	// for (int i = 0; i < c.getList().size(); i++) {
	// dataset.addValue(c.getList().get(i), "Row " + i, "2018-0" + i + 2);
	// }
	//
	// JFreeChart chart = ChartFactory.createBarChart("Elektrizitšt", // chart title
	// "Category", // domain axis label
	// "Value", // range axis label
	// dataset, // data
	// PlotOrientation.VERTICAL, // orientation
	// true, // include legend
	// true, // tooltips
	// false // URLs
	// );
	// ChartPanel chartPanel = new ChartPanel(chart, false);
	// chartPanel.setPreferredSize(new Dimension(500, 270));
	//
	// return chartPanel;
	// }

}
