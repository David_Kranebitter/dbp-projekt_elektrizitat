package Models;

import java.util.Date;

public class Country {

	private String countryName;
	private double value;
	private Date date;
	
	public Country(String countryName, double value, Date date) {
		super();
		this.countryName = countryName;
		this.value = value; 
		this.date = date;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String toString() {
		return countryName;
	}
}
